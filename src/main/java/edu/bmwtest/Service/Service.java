package edu.bmwtest.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

import edu.bmwtest.Model.Address;
import edu.bmwtest.Model.Company;
import edu.bmwtest.Model.Users;
import edu.bmwtest.Repository.UserRepository;
import edu.bmwtest.mavenproject.Main;

public class Service {

	private static void validateEmail(String email, Logger logger, int userId, String name) {
		String regexp = "^(.+)@(.+)$";
		Pattern p = Pattern.compile(regexp);
		Matcher m = p.matcher(email);
		if (!m.matches()) {
			String errorMessage = String.format("The email address of user: %s with id: %s is invalid", name, userId);
			logger.warn(errorMessage);
		}
	}

	public static void getData() throws Exception {
		Logger logger = LoggerFactory.getLogger(Main.class);
		UserRepository userRepository = new UserRepository();
		List<Users> usersBackFromServer = userRepository.getAllUsers();
		// System.out.println(usersBackFromServer.size());
		HttpResponse<JsonNode> response = Unirest.get("https://jsonplaceholder.typicode.com/users")
				.header("accept", "application/json").asJson();
		JSONArray rawData = response.getBody().getArray();
		Boolean isNrOfUsersChanged = usersBackFromServer.size() != rawData.length();
		if (response.getStatus() != 200) {
			logger.error("RESPONSE STATUS: {}", response.getStatus());
		} else {
			logger.info("RESPONSE STATUS: {}, NUMBER OF USERS HAS CHANGED: {}", response.getStatus(),
					isNrOfUsersChanged);
		}

		ArrayList<Users> listOfUsers = new ArrayList<Users>();

		for (int i = 0; i < rawData.length(); i++) {
			JSONObject rawObject = rawData.getJSONObject(i);

			int id = (Integer) rawObject.get("id");
			String name = (String) rawObject.get("name");
			String username = (String) rawObject.get("username");
			String email = (String) rawObject.get("email");

			validateEmail(email, logger, id, name);

			JSONObject address = (JSONObject) rawObject.get("address");
			String street = address.getString("street");
			String suite = address.getString("suite");
			String city = address.getString("city");
			String zipcode = address.getString("zipcode");

			JSONObject geo = (JSONObject) address.get("geo");
			String lat = geo.getString("lat");
			String lng = geo.getString("lng");
			String geoConcat = String.format("lat: %s, lng: %s.", lat, lng);

			Address addressObj = new Address();

			addressObj.setStreet(street);
			addressObj.setSuite(suite);
			addressObj.setCity(city);
			addressObj.setZipcode(zipcode);
			addressObj.setGeo(geoConcat);

			String phone = rawObject.getString("phone");
			String website = rawObject.getString("website");

			JSONObject company = (JSONObject) rawObject.get("company");
			String companyName = company.getString("name");// pay attention to name
			String catchPhrase = company.getString("catchPhrase");
			String bs = company.getString("bs");

			Company companyObj = new Company();
			companyObj.setName(companyName);
			companyObj.setCatchPhrase(catchPhrase);
			companyObj.setBs(bs);

//			String addressConcat = String.format("Street: %s, suite: %s, city: %s, zipcode: %s, geo: : %s.", street,
//					suite, city, zipcode, geoConcat);
//			String companyConcat = String.format("Name: %s, catchPhrase: %s, bs: %s.", companyName, catchPhrase, bs);

			Users userObj = new Users();
			userObj.setName(name);
			userObj.setUsername(username);
			userObj.setUserId(id);
			userObj.setEmail(email);
			userObj.setAddress(addressObj);
			userObj.setPhone(phone);
			userObj.setWebsite(website);
			userObj.setCompany(companyObj);
			// System.out.println("USER " + i +" "+ userObj.toString());
			listOfUsers.add(userObj);

		}
		if (usersBackFromServer.size() == 0 && rawData.length() > 0)
			userRepository.persistUsers(listOfUsers);

	}
}
