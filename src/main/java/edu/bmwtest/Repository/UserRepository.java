package edu.bmwtest.Repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.AbstractQuery;
import javax.persistence.criteria.CriteriaBuilder;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import edu.bmwtest.Model.Users;

public class UserRepository {
	private static SessionFactory sessionFactory;

	public void persistUsers(ArrayList<Users> listOfUsers) {

		List<Users> list = new ArrayList<Users>(listOfUsers);
		sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		session.beginTransaction();

		for (Users user : list) {
			try {
				session.save(user);
			} catch (HibernateException e) {
				e.printStackTrace();
				session.getTransaction().rollback();
			}

		}
		session.getTransaction().commit();
	}

	private static <T> List<T> loadAllData(Class<T> type, Session session) {
		session.beginTransaction();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		javax.persistence.criteria.CriteriaQuery<T> criteria = builder.createQuery(type);
		((AbstractQuery<T>) criteria).from(type);
		List<T> data = session.createQuery((javax.persistence.criteria.CriteriaQuery<T>) criteria).getResultList();
		return data;
	}

	public List<Users> getAllUsers() {
		sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		List<Users> users = loadAllData(Users.class, session);
		return users;
	}
}
