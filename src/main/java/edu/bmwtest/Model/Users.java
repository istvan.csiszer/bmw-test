package edu.bmwtest.Model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class Users {

	@Column(name = "id")
	@Id
	@GeneratedValue
	private int id;

	@Column(name = "userid")
	private int userid;

	@Column(name = "name")
	private String name;

	@Column(name = "username")
	private String username;

	@Column(name = "email")
	private String email;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, targetEntity = edu.bmwtest.Model.Address.class)
	private Address address;

	@Column(name = "phone")
	private String phone;

	@Column(name = "website")
	private String website;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, targetEntity = edu.bmwtest.Model.Company.class)
	private Company company;

	public int getUserId() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public int getId() {
		return id;
	}

	public void setUserId(int id) {
		this.userid = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Override
	public String toString() {
		return "User id=" + userid + ", name=" + name + ", username=" + username + ", email=" + email + " address="
				+ address.toString() + ", phone=" + phone + ", website=" + website + ", company=" + company.toString()
				+ "]";
	}
}