package edu.bmwtest.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "company")
public class Company {
	
	@Column(name = "userid")
	@Id
	@GeneratedValue
	private int companyId;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "catchPhrase")
	private String catchPhrase;
	
	@Column(name = "bs")
	private String bs;
	
	public int getCompanyId() {
		return companyId;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCatchPhrase() {
		return catchPhrase;
	}
	public void setCatchPhrase(String catchPhrase) {
		this.catchPhrase = catchPhrase;
	}
	public String getBs() {
		return bs;
	}
	public void setBs(String bs) {
		this.bs = bs;
	}
	
	@Override
	public String toString() {
		return "Company [companyId=" + companyId + ", name=" + name + ", catchPhrase=" + catchPhrase + ", bs=" + bs
				+ "]";
	}	
}
